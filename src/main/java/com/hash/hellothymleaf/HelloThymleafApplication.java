package com.hash.hellothymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloThymleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloThymleafApplication.class, args);
    }
}
