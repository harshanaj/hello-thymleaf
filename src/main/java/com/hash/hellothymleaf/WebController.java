package com.hash.hellothymleaf;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
public class WebController {

    @RequestMapping("/hello/{name}")
    public String hello(Map model, @PathVariable String name) {
        model.put("name", name);
        return "helloThymeleaf"; // This should be a thymeleaf page name
    }
}
